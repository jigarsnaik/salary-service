### Salary Service

    Retruns the employee list with all attribute as json string format. 
    Returns the salary summary of all employees. Summary includes total fixed salary paid, total commission paid and sum of Commission + fixed salary paid. 

### Unit Tests

    Unit tests convers both individual visitors and the over all execution.

### Design Pattern Used.

    Visitor Pattern

    All the model classes are under package com.jigarnaik.salaryservice.model
    Both the visitors are under package com.jigarnaik.salaryservice.visitors
    Main class com.jigarnaik.salaryservice.SalaryServiceApplication

## Class diagram

<img src="salary-service.png" alt="Class Diagram"/>

## Sample output for all salaries

    [
        {
            "name": "Jigar Naik",
            "department": "IT",
            "emailId": "jnaik@gmail.com",
            "salary": 4000.0
        }, {
            "name": "Jagu Naik",
            "department": "IT",
            "emailId": "jjnaik@gmail.com",
            "salary": 4500.0
        }, {
            "name": "Vikram Naik",
            "department": "Sales",
            "emailId": "vnaik@gmail.com",
            "commRate": 10.0,
            "saleVolume": 5000.0
        }, {
            "name": "Swati Naik",
            "department": "Sales",
            "emailId": "snaik@gmail.com",
            "commRate": 9.0,
            "saleVolume": 6000.0
        }, {
            "name": "Drashti Naik",
            "department": "Sales",
            "emailId": "pnaik@gmail.com",
            "salary": 2000.0,
            "commRate": 8.0,
            "saleVolume": 3000.0
        }, {
            "name": "Parth Naik",
            "department": "Sales",
            "emailId": "dnaik@gmail.com",
            "salary": 2500.0,
            "commRate": 8.0,
            "saleVolume": 3200.0
        }
    ]