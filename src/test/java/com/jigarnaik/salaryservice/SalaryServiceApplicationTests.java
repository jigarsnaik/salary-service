package com.jigarnaik.salaryservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jigarnaik.salaryservice.model.*;
import com.jigarnaik.salaryservice.visitors.JsonVisitor;
import com.jigarnaik.salaryservice.visitors.SalarySummaryVisitor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class SalaryServiceApplicationTests {

    private SalarySummaryVisitor summaryVisitor = null;
    private SalarySummaryVisitor summaryVisitor1 = null;
    private JsonVisitor jsonVisitor = null;
    private JsonVisitor jsonVisitor1 = null;

    @BeforeEach
    public void setUp() {

        summaryVisitor = new SalarySummaryVisitor();
        summaryVisitor1 = new SalarySummaryVisitor();
        jsonVisitor = new JsonVisitor(new ObjectMapper());
        jsonVisitor1 = new JsonVisitor(new ObjectMapper());

        List<Visitable> employees = new ArrayList<>();
        employees.add(new EmployeeWithFixedSalary("Jigar Naik", "IT", "jnaik@gmail.com", 4000));
        employees.add(new EmployeeWithFixedSalary("Jagu Naik", "IT", "jjnaik@gmail.com", 4500));
        employees.add(new EmployeeWithCommission("Vikram Naik", "Sales", "vnaik@gmail.com", 10, 5000));
        employees.add(new EmployeeWithCommission("Swati Naik", "Sales", "snaik@gmail.com", 9, 6000));
        employees.add(new EmployeeWithMixedSalary("Drashti Naik", "Sales", "pnaik@gmail.com", 2000, 8, 3000));
        employees.add(new EmployeeWithMixedSalary("Parth Naik", "Sales", "dnaik@gmail.com", 2500, 8, 3200));

        employees.stream().forEach(e -> {
            e.accept(summaryVisitor);
            e.accept(jsonVisitor);
        });
    }

    @Test
    void testSummary() {
        Summary summary = summaryVisitor.salarySummary();
        assertNotNull(summary);
        assertEquals(166600.0, summary.getTotalSalaryAndCommissions());
        assertEquals(13000.0, summary.getTotalFixedSalary());
        assertEquals(153600.0, summary.getTotalCommissions());
    }

    @Test
    void testPrintSalary() throws URISyntaxException, IOException {
        String salariesInJsonFormat = jsonVisitor.printSalaries();
        String expected = Files.lines(Paths.get(getClass().getClassLoader().getResource("expectedSalaries.json")
                .toURI())).collect(Collectors.joining("\n"));
        assertEquals(expected, salariesInJsonFormat);
    }

    @Test
    void testPrintSalary_withoutCallingVisitor() {
        String salariesInJsonFormat = jsonVisitor1.printSalaries();
        assertEquals("[]", salariesInJsonFormat);
    }

    @Test
    void testSummary_withoutCallingVisitor() {
        Summary summary = summaryVisitor1.salarySummary();
        assertNotNull(summary);
        double expected = 0.0;
        assertEquals(expected, summary.getTotalCommissions());
        assertEquals(expected, summary.getTotalSalaryAndCommissions());
        assertEquals(expected, summary.getTotalFixedSalary());
    }

}
