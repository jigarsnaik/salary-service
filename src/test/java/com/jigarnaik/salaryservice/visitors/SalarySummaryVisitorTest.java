package com.jigarnaik.salaryservice.visitors;

import com.jigarnaik.salaryservice.model.EmployeeWithCommission;
import com.jigarnaik.salaryservice.model.EmployeeWithFixedSalary;
import com.jigarnaik.salaryservice.model.EmployeeWithMixedSalary;
import com.jigarnaik.salaryservice.model.Summary;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SalarySummaryVisitorTest {

    private SalarySummaryVisitor visitor;
    private EmployeeWithFixedSalary employeeWithFixedSalary;
    private EmployeeWithCommission employeeWithCommission;
    private EmployeeWithMixedSalary employeeWithMixedSalary;

    @BeforeEach
    public void setUp() {
        this.visitor = new SalarySummaryVisitor();
        employeeWithFixedSalary = new EmployeeWithFixedSalary("Jigar", "IT", "jnaik@gmail.com", 200);
        employeeWithCommission = new EmployeeWithCommission("Jagu", "IT", "jjnaik@gmail.com", 10, 10);
        employeeWithMixedSalary = new EmployeeWithMixedSalary("Swati", "IT", "snaik@gmail.com", 100, 10, 5);
    }

    @Test
    void visit_forFixedSalary() {
        visitor.visit(employeeWithFixedSalary);
        Summary summary = visitor.salarySummary();
        Assertions.assertNotNull(summary);
        double none = 0.0;
        assertEquals(none, summary.getTotalCommissions());
        assertEquals(200.00, summary.getTotalSalaryAndCommissions());
        assertEquals(200.00, summary.getTotalFixedSalary());
    }

    @Test
    void visit_forCommission() {
        visitor.visit(employeeWithCommission);
        Summary summary = visitor.salarySummary();
        Assertions.assertNotNull(summary);
        double none = 0.0;
        assertEquals(100.00, summary.getTotalCommissions());
        assertEquals(100.00, summary.getTotalSalaryAndCommissions());
        assertEquals(none, summary.getTotalFixedSalary());
    }

    @Test
    void visit_forMixedSalary() {
        visitor.visit(employeeWithMixedSalary);
        Summary summary = visitor.salarySummary();
        Assertions.assertNotNull(summary);
        double none = 0.0;
        assertEquals(50.00, summary.getTotalCommissions());
        assertEquals(150.00, summary.getTotalSalaryAndCommissions());
        assertEquals(100, summary.getTotalFixedSalary());
    }
}