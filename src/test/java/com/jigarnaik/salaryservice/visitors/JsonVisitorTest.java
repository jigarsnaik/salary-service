package com.jigarnaik.salaryservice.visitors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jigarnaik.salaryservice.exception.JsonConversionException;
import com.jigarnaik.salaryservice.model.EmployeeWithCommission;
import com.jigarnaik.salaryservice.model.EmployeeWithFixedSalary;
import com.jigarnaik.salaryservice.model.EmployeeWithMixedSalary;
import com.jigarnaik.salaryservice.model.Visitable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ExtendWith(MockitoExtension.class)
class JsonVisitorTest {

    private JsonVisitor visitor;
    private EmployeeWithFixedSalary employeeWithFixedSalary;
    private EmployeeWithCommission employeeWithCommission;
    private EmployeeWithMixedSalary employeeWithMixedSalary;
    @Mock
    private ObjectMapper mapperMock;

    @BeforeEach
    public void setUp() {
        visitor = new JsonVisitor(new ObjectMapper());
        List<Visitable> salaries = new ArrayList<>();
        employeeWithFixedSalary = new EmployeeWithFixedSalary("Jigar", "IT", "jnaik@gmail.com", 200);
        employeeWithCommission = new EmployeeWithCommission("Jagu", "IT", "jjnaik@gmail.com", 10, 10);
        employeeWithMixedSalary = new EmployeeWithMixedSalary("Swati", "IT", "snaik@gmail.com", 100, 10, 5);
    }

    @Test
    void testVisit_forFixedSalary() {
        visitor.visit(employeeWithFixedSalary);
        String actual = visitor.printSalaries();
        Assertions.assertEquals(getFileContent("expectedEmployeeWithFixedSalary.json"), actual);
    }

    @Test
    void testVisit_forFixedSalary_throwsException() throws JsonProcessingException {
        visitor = new JsonVisitor(mapperMock);
        Mockito.when(mapperMock.writeValueAsString(Mockito.any())).thenThrow(new JsonConversionException("Exception while converting EmployeeWithFixedSalary to JSON.", null));
        JsonConversionException exception = Assertions.assertThrows(JsonConversionException.class, () -> visitor.visit(employeeWithFixedSalary));
        Assertions.assertEquals("Exception while converting EmployeeWithFixedSalary to JSON.", exception.getMessage());
    }

    @Test
    void testVisit_forCommission() {
        visitor.visit(employeeWithCommission);
        String actual = visitor.printSalaries();
        Assertions.assertEquals(getFileContent("expectedEmployeeWithCommissions.json"), actual);
    }

    @Test
    void testVisit_forCommission_throwsException() throws JsonProcessingException {
        visitor = new JsonVisitor(mapperMock);
        Mockito.when(mapperMock.writeValueAsString(Mockito.any())).thenThrow(new JsonConversionException("Exception while converting EmployeeWithCommission to JSON.", null));
        JsonConversionException exception = Assertions.assertThrows(JsonConversionException.class, () -> visitor.visit(employeeWithCommission));
        Assertions.assertEquals("Exception while converting EmployeeWithCommission to JSON.", exception.getMessage());
    }

    @Test
    void testVisit_forMixed() {
        visitor.visit(employeeWithMixedSalary);
        String actual = visitor.printSalaries();
        Assertions.assertEquals(getFileContent("expectedEmployeeWithMixedSalary.json"), actual);
    }

    @Test
    void testVisit_forMixed_throwsException() throws JsonProcessingException {
        visitor = new JsonVisitor(mapperMock);
        Mockito.when(mapperMock.writeValueAsString(Mockito.any())).thenThrow(new JsonConversionException("Exception while converting EmployeeWithMixedSalary to JSON.", null));
        JsonConversionException exception = Assertions.assertThrows(JsonConversionException.class, () -> visitor.visit(employeeWithMixedSalary));
        Assertions.assertEquals("Exception while converting EmployeeWithMixedSalary to JSON.", exception.getMessage());
    }

    public String getFileContent(String fileName) {
        try {
            return Files.lines(Paths.get(getClass().getClassLoader().getResource(fileName).toURI())).collect(Collectors.joining("\n"));
        } catch (Exception e) {
            System.out.println("file name : " + fileName);
            e.printStackTrace();
        }
        return null;
    }


}