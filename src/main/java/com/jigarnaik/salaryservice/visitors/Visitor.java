package com.jigarnaik.salaryservice.visitors;

import com.jigarnaik.salaryservice.model.EmployeeWithCommission;
import com.jigarnaik.salaryservice.model.EmployeeWithFixedSalary;
import com.jigarnaik.salaryservice.model.EmployeeWithMixedSalary;

public interface Visitor {

    void visit(EmployeeWithCommission commSalary);

    void visit(EmployeeWithFixedSalary fixedSalary);

    void visit(EmployeeWithMixedSalary mixSalary);
}

