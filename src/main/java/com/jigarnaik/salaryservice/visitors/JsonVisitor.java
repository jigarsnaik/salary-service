package com.jigarnaik.salaryservice.visitors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jigarnaik.salaryservice.exception.JsonConversionException;
import com.jigarnaik.salaryservice.model.EmployeeWithCommission;
import com.jigarnaik.salaryservice.model.EmployeeWithFixedSalary;
import com.jigarnaik.salaryservice.model.EmployeeWithMixedSalary;

import java.util.ArrayList;
import java.util.List;

/**
 * Responsible for converting employee salary object into json format for printing.
 */
public class JsonVisitor implements Visitor {

    private final List<String> salaries = new ArrayList<>();
    private final ObjectMapper mapper;

    public JsonVisitor(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public void visit(EmployeeWithCommission salary) {
        try {
            salaries.add(mapper.writeValueAsString(salary));
        } catch (JsonProcessingException e) {
            throw new JsonConversionException("Exception while converting " + EmployeeWithCommission.class.getSimpleName() + " to JSON.", e);
        }
    }

    @Override
    public void visit(EmployeeWithFixedSalary salary) {
        try {
            salaries.add(mapper.writeValueAsString(salary));
        } catch (JsonProcessingException e) {
            throw new JsonConversionException("Exception while converting " + EmployeeWithFixedSalary.class.getSimpleName() + " to JSON.", e);
        }
    }

    @Override
    public void visit(EmployeeWithMixedSalary salary) {
        try {
            salaries.add(mapper.writeValueAsString(salary));
        } catch (JsonProcessingException e) {
            throw new JsonConversionException("Exception while converting " + EmployeeWithMixedSalary.class.getSimpleName() + " to JSON.", e);
        }
    }

    /**
     * Return JSON String representing all employees attributes.
     *
     * @return JSON String
     */
    public String printSalaries() {
        return salaries.toString();
    }
}
