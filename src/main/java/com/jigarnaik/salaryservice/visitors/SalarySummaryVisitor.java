package com.jigarnaik.salaryservice.visitors;

import com.jigarnaik.salaryservice.model.EmployeeWithCommission;
import com.jigarnaik.salaryservice.model.EmployeeWithFixedSalary;
import com.jigarnaik.salaryservice.model.EmployeeWithMixedSalary;
import com.jigarnaik.salaryservice.model.Summary;

/**
 * Visitor class responsible for calculating summary statistics for all employees.
 * This can be modified to calculate other indicators, for example, avg, max, min etc.
 */
public class SalarySummaryVisitor implements Visitor {

    private double sumOfCommissions;
    private double sumOfFixedSalary;

    @Override
    public void visit(EmployeeWithCommission salary) {
        sumOfCommissions += salary.getCommRate() * salary.getSaleVolume();
    }

    @Override
    public void visit(EmployeeWithFixedSalary salary) {
        sumOfFixedSalary += salary.getSalary();
    }

    @Override
    public void visit(EmployeeWithMixedSalary salary) {
        sumOfCommissions += salary.getCommRate() * salary.getSaleVolume();
        sumOfFixedSalary += salary.getSalary();
    }

    public Summary salarySummary() {
        return new Summary(sumOfFixedSalary, sumOfCommissions, sumOfFixedSalary + sumOfCommissions);
    }

}
