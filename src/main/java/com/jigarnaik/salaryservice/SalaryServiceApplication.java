package com.jigarnaik.salaryservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jigarnaik.salaryservice.model.EmployeeWithCommission;
import com.jigarnaik.salaryservice.model.EmployeeWithFixedSalary;
import com.jigarnaik.salaryservice.model.EmployeeWithMixedSalary;
import com.jigarnaik.salaryservice.model.Visitable;
import com.jigarnaik.salaryservice.visitors.JsonVisitor;
import com.jigarnaik.salaryservice.visitors.SalarySummaryVisitor;

import java.util.ArrayList;
import java.util.List;

public class SalaryServiceApplication {
    private static final ObjectMapper mapper = new ObjectMapper();
    private static final String DEPT_IT = "IT";
    private static final String DEPT_SALES = "Sales";

    public static void main(String[] args) throws JsonProcessingException {
        var calcVisitor = new SalarySummaryVisitor();
        var jsonVisitor = new JsonVisitor(new ObjectMapper());

        List<Visitable> employees = new ArrayList<>();
        employees.add(new EmployeeWithFixedSalary("Jigar Naik", DEPT_IT, "jigarsnaik@gmail.com", 4000));
        employees.add(new EmployeeWithFixedSalary("Jagu Naik", DEPT_IT, "jagrutijigarnaik@gmail.com", 4500));
        employees.add(new EmployeeWithCommission("Vikram Naik", DEPT_SALES, "vikrambnaik@gmail.com", 10, 5000));
        employees.add(new EmployeeWithCommission("Swati Naik", DEPT_SALES, "swati@gmail.com", 9, 6000));
        employees.add(new EmployeeWithMixedSalary("Drashti Naik", DEPT_SALES, "parth@gmail.com", 2000, 8, 3000));
        employees.add(new EmployeeWithMixedSalary("Parth Naik", DEPT_SALES, "drashti@gmail.com", 2500, 8, 3200));

        employees.stream().forEach(e -> {
            e.accept(calcVisitor);
            e.accept(jsonVisitor);
        });

        System.out.println(jsonVisitor.printSalaries());
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(calcVisitor.salarySummary()));
    }
}
