package com.jigarnaik.salaryservice.model;

import com.jigarnaik.salaryservice.visitors.Visitor;

/**
 * Represent all employees with fixed salary and commission.
 */
public class EmployeeWithMixedSalary extends Employee {

    private final double salary;
    private final double commRate;
    private final double saleVolume;

    public EmployeeWithMixedSalary(String name, String department, String emailId, double salary, double commRate, double saleVolume) {
        super(name, department, emailId);
        this.salary = salary;
        this.commRate = commRate;
        this.saleVolume = saleVolume;
    }

    public double getSalary() {
        return salary;
    }

    public double getCommRate() {
        return commRate;
    }

    public double getSaleVolume() {
        return saleVolume;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
