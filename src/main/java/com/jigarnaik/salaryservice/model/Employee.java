package com.jigarnaik.salaryservice.model;

/**
 * Abstract class represent the common attributes of employees
 */
public abstract class Employee implements Visitable {

    private final String name;
    private final String department;
    private final String emailId;

    protected Employee(String name, String department, String emailId) {
        this.name = name;
        this.department = department;
        this.emailId = emailId;
    }

    public String getName() {
        return name;
    }

    public String getDepartment() {
        return department;
    }

    public String getEmailId() {
        return emailId;
    }

}
