package com.jigarnaik.salaryservice.model;

import com.jigarnaik.salaryservice.visitors.Visitor;

/**
 * Represent all employee with commissions.
 */
public class EmployeeWithCommission extends Employee {

    private final double commRate;
    private final double saleVolume;

    public EmployeeWithCommission(String name, String department, String emailId, double commRate, double saleVolume) {
        super(name, department, emailId);
        this.commRate = commRate;
        this.saleVolume = saleVolume;
    }

    public double getCommRate() {
        return commRate;
    }

    public double getSaleVolume() {
        return saleVolume;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

}
