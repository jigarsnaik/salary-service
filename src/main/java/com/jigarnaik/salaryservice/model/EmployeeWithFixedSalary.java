package com.jigarnaik.salaryservice.model;

import com.jigarnaik.salaryservice.visitors.Visitor;

/**
 * Represent all employees with fixed salary.
 */
public class EmployeeWithFixedSalary extends Employee {

    private final double salary;

    public EmployeeWithFixedSalary(String name, String department, String emailId, double salary) {
        super(name, department, emailId);
        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
