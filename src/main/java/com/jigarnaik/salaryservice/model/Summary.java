package com.jigarnaik.salaryservice.model;

/**
 * Summary class gives summary information about the salary.
 */
public class Summary {

    private final double totalFixedSalary;
    private final double totalCommissions;
    private final double totalSalaryAndCommissions;

    public Summary(double totalFixedSalary, double totalCommissions, double totalSalaryAndCommissions) {
        this.totalFixedSalary = totalFixedSalary;
        this.totalCommissions = totalCommissions;
        this.totalSalaryAndCommissions = totalSalaryAndCommissions;
    }

    /**
     * Gives total fixed salary paid to all employees
     *
     * @return double
     */
    public double getTotalFixedSalary() {
        return totalFixedSalary;
    }

    /**
     * Gives the total amount paid as a commission for all employees
     *
     * @return double
     */
    public double getTotalCommissions() {
        return totalCommissions;
    }

    /**
     * Gives total amoutn paid as a fixed salary plus commission
     *
     * @return double
     */
    public double getTotalSalaryAndCommissions() {
        return totalSalaryAndCommissions;
    }
}
