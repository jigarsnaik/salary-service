package com.jigarnaik.salaryservice.model;

import com.jigarnaik.salaryservice.visitors.Visitor;

/**
 * Indicates implementing model class is Visitable.
 */
public interface Visitable {

    void accept(Visitor salaryCalcVisitor);
}
