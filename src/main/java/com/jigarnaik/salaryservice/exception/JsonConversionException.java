package com.jigarnaik.salaryservice.exception;

/**
 * Exception will be thrown when service fail conversion of java object to json object
 */
public class JsonConversionException extends RuntimeException {

    public JsonConversionException(String message, Throwable e) {
        super(message, e);
    }
}
